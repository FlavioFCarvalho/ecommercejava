package com.ecommerce.teste;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ecommerce.cliente.Cliente;
import com.ecommerce.cliente.ClienteRN;
import com.ecommerce.produto.Produto;
import com.ecommerce.produto.ProdutoRN;
import com.ecommerce.venda.Venda;
import com.ecommerce.venda.VendaRN;

public class VendaTest extends TestHeranca {
	
	//Criação da variaveis globais para poderem ser acessadas registraTest
	Cliente c1;
	Cliente c2;
	Cliente c3;
	
	Produto p1;
	Produto p2;
	Produto p3;

	@Before
	public void setup() {
		c1 = new Cliente("09352683161", "teste1@mail", "Rua 1",
				"Cliente1", new Date(), 4000);
		c2 = new Cliente("09352683161", "teste2@mail", "Rua 1",
				"Cliente2", new Date(), 3000);
		c3 = new Cliente("09352683161", "teste3@mail", "Rua 1",
				"Cliente3", new Date(), 2000);

		ClienteRN clienteRN = new ClienteRN();
		clienteRN.salvar(c1);
		clienteRN.salvar(c2);
		clienteRN.salvar(c3);

		p1 = new Produto("litro", "coca cola", new Date(), 100, 3.5f);
		p2 = new Produto("litr", "fanta", new Date(), 100, 3.5f);
		p3 = new Produto("lit", "pepsi", new Date(), 100, 3.5f);

		ProdutoRN produtoRN = new ProdutoRN();

		produtoRN.salvar(p1);
		produtoRN.salvar(p2);
		produtoRN.salvar(p3);

	}
	@After
	public void limpaBanco() {
		
		VendaRN vendaRN = new VendaRN();

		List<Venda> vendas = vendaRN.listar();

		for (Venda venda : vendas) {
			vendaRN.excluir(venda);
		}
	}

	@Test
	public void registraTest() {
		
		VendaRN vendaRN = new VendaRN();
		
		Venda venda1 = new Venda();
		
		venda1.setCliente(c1);
		venda1.setProduto(p1);
		venda1.setDataVenda(new Date());
		
		Venda venda2 = new Venda();
		
		venda2.setCliente(c2);
		venda2.setProduto(p2);
		venda2.setDataVenda(new Date());
		
		Venda venda3 = new Venda();
		
		venda3.setCliente(c3);
		venda3.setProduto(p3);
		venda3.setDataVenda(new Date());

		vendaRN.registraVenda(venda1);
		vendaRN.registraVenda(venda2);
		vendaRN.registraVenda(venda3);
		
		List<Venda> vendas = vendaRN.listar();
		
		assertEquals(3, vendas.size());
	}

}
