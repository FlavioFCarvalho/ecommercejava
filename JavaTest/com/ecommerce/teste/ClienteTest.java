package com.ecommerce.teste;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ecommerce.cliente.Cliente;
import com.ecommerce.cliente.ClienteRN;


public class ClienteTest extends TestHeranca{

	
	@Before
	public void setup() {
		Cliente c1 = new Cliente("09352683161", "teste1@mail", "Rua 1",
				"Cliente1", new Date(), 4000);
		Cliente c2 = new Cliente("09352683161", "teste2@mail", "Rua 1",
				"Cliente2", new Date(), 3000);
		Cliente c3 = new Cliente("09352683161", "teste3@mail", "Rua 1",
				"Cliente3", new Date(), 2000);

		ClienteRN clienteRN = new ClienteRN();
		clienteRN.salvar(c1);
		clienteRN.salvar(c2);
		clienteRN.salvar(c3);
	}

	@After
	public void limpaBanco() {

		ClienteRN clienteRN = new ClienteRN();
		List<Cliente> lista = clienteRN.listar();

		for (Cliente cliente : lista) {
			clienteRN.excluir(cliente);
		}
	}

	@Test
	public void salvarTest() {

		Cliente c1 = new Cliente();
		c1.setNome("Flávio Carvalho");
		c1.setEndereco("Rua A");
		c1.setRenda(5000f);
		c1.setCpf("0007788788");

		ClienteRN clienteRN = new ClienteRN();

		clienteRN.salvar(c1);

		assertEquals(true, true);

	}

	@Test
	public void listarTest() {
		ClienteRN clienteRN = new ClienteRN();
		List<Cliente> lista = clienteRN.listar();
		assertEquals(3, lista.size());
	}

	@Test
	public void excluirTest() {
		ClienteRN clienteRN = new ClienteRN();

		List<Cliente> lista = clienteRN.listar();

		Cliente clienteExcluido = lista.get(0);

		clienteRN.excluir(clienteExcluido);

		lista = clienteRN.listar();

		assertEquals(2, lista.size());

	}
	
	@Test
	public void pesquisaTest(){
		ClienteRN clienteRN = new ClienteRN();
		//pesquisa um cliente em qualquer posição
		Cliente clientePesquisado = clienteRN.pesquisar("te2");
		
		//compara pelo email
		assertEquals("teste2@mail",clientePesquisado.getEmail());
		
	}
	
	@Test
	public void alterarTest(){
		ClienteRN clienteRN = new ClienteRN();
		//pesquisa um cliente em qualquer posição
		Cliente clientePesquisado = clienteRN.pesquisar("te2");
		
		assertEquals("teste2@mail",clientePesquisado.getEmail());
		
		clientePesquisado.setEndereco("Novo Endereço");
		
		clienteRN.alterar(clientePesquisado);
		
		Cliente clienteAlterado = clienteRN.pesquisar("te2");
		
		assertEquals("Novo Endereço", clienteAlterado.getEndereco());
		
		
	}

}
