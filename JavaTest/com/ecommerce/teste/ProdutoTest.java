package com.ecommerce.teste;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;

import org.junit.Before;
import org.junit.Test;

import com.ecommerce.produto.Produto;
import com.ecommerce.produto.ProdutoRN;

public class ProdutoTest extends TestHeranca {

	@Before
	public void setup() {
		Produto p1 = new Produto("litro", "coca cola", new Date(), 100, 3.5f);
		Produto p2 = new Produto("litr", "fanta", new Date(), 100, 3.5f);
		Produto p3 = new Produto("lit", "pepsi", new Date(), 100, 3.5f);

		ProdutoRN produtoRN = new ProdutoRN();

		produtoRN.salvar(p1);
		produtoRN.salvar(p2);
		produtoRN.salvar(p3);
	}

	@After
	public void limpaBanco() {
		// Criteria lista = sessao.createCriteria(Produto.class);
		// @SuppressWarnings("unchecked")

		ProdutoRN produtoRN = new ProdutoRN();

		List<Produto> produtos = produtoRN.listar();

		for (Produto produto : produtos) {
			produtoRN.excluir(produto);
		}
	}

	@Test
	public void salvarProdutoTest() {
		// Query consulta = pesquisar("co");
		// Produto produtoPesquisado = (Produto) consulta.uniqueResult();

		ProdutoRN produtoRN = new ProdutoRN();

		Produto produtoSalvo = new Produto("lote33", "Apontador", new Date(),
				100, 3.5f);

		produtoRN.salvar(produtoSalvo);

		Produto produtoPesquisado = produtoRN.pesquisarPorNome("Ap");
		assertEquals("lote33", produtoPesquisado.getUnidade());

	}

	@Test
	public void listaProdutos() {

		// Criteria lista = sessao.createCriteria(Produto.class);
		// @SuppressWarnings("unchecked")

		ProdutoRN produtoRN = new ProdutoRN();

		List<Produto> produtos = produtoRN.listar();

		assertEquals(3, produtos.size());
	}

	// mesma coisa do alterar cliente
	// @Test
	// public void excluirProdutoTest() {
	// Query consulta = pesquisar("fanta");
	// Produto produtoDeletado = (Produto) consulta.uniqueResult();
	// sessao.delete(produtoDeletado);
	//
	// produtoDeletado = (Produto) consulta.uniqueResult();
	//
	// assertNull(produtoDeletado);
	// }

	// mesma coisa do alterar cliente
	// @Test
	// public void alteracaoProdutoTest() {
	// Query consulta = pesquisar("coca cola");
	// Produto produtoAlterado = (Produto) consulta.uniqueResult();
	// produtoAlterado.setEstoque(100);
	//
	// sessao.update(produtoAlterado);
	//
	// produtoAlterado = (Produto) consulta.uniqueResult();
	//
	// assertEquals(100, produtoAlterado.getEstoque().intValue());
	//
	// }

	// private Query pesquisar(String parametro) {
	// String sql = "from Produto p where p.descricao like:descricao";
	// Query consulta = sessao.createQuery(sql);
	// consulta.setString("descricao", "%" + parametro + "%");
	// return consulta;
	// }

}