package com.ecommerce.venda;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.ecommerce.cliente.Cliente;
import com.ecommerce.cliente.ClienteRN;
import com.ecommerce.produto.Produto;
import com.ecommerce.produto.ProdutoRN;

@ManagedBean(name = "registroVendas")
@ViewScoped
public class RegistraVendaBean {

	private Cliente clienteSelecionado;

	private Produto produtoSelecionado = new Produto();

	private List<Produto> carrinhoCompras = new ArrayList<Produto>();

	private List<SelectItem> clientesSelect;

	private float valorTotal;

	public String buscarProduto() {

		ProdutoRN produtoRN = new ProdutoRN();
		Produto produtoPesquisado = new Produto();

		if (this.produtoSelecionado.getDescricao() != null
				&& !this.produtoSelecionado.getDescricao().equals("")) {
			produtoPesquisado = produtoRN
					.pesquisarPorNome(this.produtoSelecionado.getDescricao());
			if (produtoPesquisado != null) {

				this.carrinhoCompras.add(produtoPesquisado);
				calculaTotal();
			}
		}
		return null;
	}

	private void calculaTotal() {

		this.valorTotal=0;
		
		if (!this.carrinhoCompras.isEmpty()) {

			for (Produto p : this.carrinhoCompras) {
				valorTotal += p.getValor();
			}

		}

	}
	
	public String finalizarVenda(){
		
		if(!this.carrinhoCompras.isEmpty()){
			ArrayList<Venda> vendas = new ArrayList<Venda>();
			VendaRN vendaRN = new VendaRN();
			
			for (Produto produto : this.carrinhoCompras) {
									
				if(this.clienteSelecionado != null){
					if(vendaRN.existeEstoque(produto)){
						vendas.add(new Venda(produto,this.clienteSelecionado));
					}
					
				}
				
			}
			for (Venda venda : vendas) {
				
				venda.setDataVenda(new Date());
				vendaRN.registraVenda(venda);
				vendaRN.reduzEstoqueProduto(venda.getProduto());
				
			}
		}
		
		
		return null;
	}

	
	public String excluirProdutoCarrinho(){
        
        if(this.carrinhoCompras != null && !this.carrinhoCompras.isEmpty()){
                if(this.produtoSelecionado != null){
                        this.carrinhoCompras.remove(this.produtoSelecionado);
                        calculaTotal();
                }
        }
        
        return null;
}

	public Cliente getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(Cliente clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}

	public List<Produto> getCarrinhoCompras() {
		return carrinhoCompras;
	}

	public void setCarrinhoCompras(List<Produto> carrinhoCompras) {
		this.carrinhoCompras = carrinhoCompras;
	}

	public float getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(float valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<SelectItem> getClientesSelect() {

		if (clientesSelect == null) {

			clientesSelect = new ArrayList<SelectItem>();

			ClienteRN clienteRN = new ClienteRN();

			List<Cliente> listaClientes = clienteRN.listar();

			if (listaClientes != null && !listaClientes.isEmpty()) {
				SelectItem item;

				for (Cliente clienteLista : listaClientes) {

					item = new SelectItem(clienteLista, clienteLista.getNome());
					clientesSelect.add(item);

				}
			}

		}
		return clientesSelect;
	}

}
