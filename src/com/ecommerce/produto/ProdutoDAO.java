package com.ecommerce.produto;

import java.util.List;

public interface ProdutoDAO {
	
	
	public void salvar(Produto produto);

	public List<Produto> listar();

	public void excluir(Produto produto);

	public Produto pesquisarPorDescricao(String descricao);

}
