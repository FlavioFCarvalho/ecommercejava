package com.ecommerce.cliente;

import java.util.List;

import com.ecommerce.util.DAOFactory;

public class ClienteRN {

	private ClienteDAO clienteDAO;

	public ClienteRN() {

		this.clienteDAO = DAOFactory.criaClienteDAO();
	}

	public void salvar(Cliente c1) {
		this.clienteDAO.salvar(c1);

	}

	public List<Cliente> listar() {
		
		return this.clienteDAO.listar();
	}

	public void excluir(Cliente cliente) {
		
		this.clienteDAO.excluir(cliente);
	}

	public Cliente pesquisar(String string) {
		
		//esse return existe pq estou buscando um cliente específico
		return this.clienteDAO.pesquisar(string);			
	}

	public void alterar(Cliente cliente) {
		
		this.clienteDAO.alterar(cliente);
		
	}

	public Object pesquisarPorCodigo(Integer codigo) {
		
		return this.clienteDAO.pesquisarPorCodigo(codigo);
	}
	
	

}
