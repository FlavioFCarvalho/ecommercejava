package com.ecommerce.util;

import com.ecommerce.cliente.ClienteDAO;
import com.ecommerce.cliente.ClienteDAOHibernate;
import com.ecommerce.produto.ProdutoDAO;
import com.ecommerce.produto.ProdutoDAOHibernate;
import com.ecommerce.venda.VendaDAO;
import com.ecommerce.venda.VendaDAOHibernate;

public class DAOFactory {

	public static ClienteDAO criaClienteDAO() {

		ClienteDAOHibernate clienteDAOHibernate = new ClienteDAOHibernate();
		clienteDAOHibernate.setSessao(HibernateUtil.getSession()
				.getCurrentSession());
		return clienteDAOHibernate;
	}

	public static ProdutoDAO criaProdutoDAO() {

		ProdutoDAOHibernate produtoDAOHibernate = new ProdutoDAOHibernate();
		produtoDAOHibernate.setSessao(HibernateUtil.getSession()
				.getCurrentSession());
		return produtoDAOHibernate;
	}

	public static VendaDAO criaVendaDAO() {
		
		VendaDAOHibernate vendaDAOHibernate = new VendaDAOHibernate();
		vendaDAOHibernate.setSessao(HibernateUtil.getSession()
				.getCurrentSession());
		return vendaDAOHibernate;
	}

	
}
